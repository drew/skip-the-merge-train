require 'rubygems'
require 'httparty'
require 'dotenv'

Dotenv.load

Dotenv.require_keys("SKIP_TRAIN_PROJECT_ID", "SKIP_TRAIN_ACCESS_TOKEN", "SKIP_TRAIN_GITLAB_HOST")

project_id                 = ENV['SKIP_TRAIN_PROJECT_ID']
access_token               = ENV['SKIP_TRAIN_ACCESS_TOKEN']
gitlab_host                = ENV['SKIP_TRAIN_GITLAB_HOST']
train_target_branch_name   = ENV['SKIP_TRAIN_TARGET_BRANCH'] || 'main'
changes_file_path          = ENV['SKIP_TRAIN_CONFIG_FILE_PATH'] || 'changes'

branch_time_stamp = Time.now.utc.to_s.gsub(' ', '_').gsub(':', '_').gsub('-', '_')
commit_target_branch_name  = "new_branch-#{branch_time_stamp}"
commit_message             = 'My changes [skip ci]'

auth_headers               = { 'Authorization' => "Bearer #{access_token}" }
changes_content            = File.read(changes_file_path)

############################
## Committing a file to a new branch, instead of the train target branch.
## This can be skipped if the changes are on the branch already

commit_request_body = {
  branch: commit_target_branch_name,
  commit_message: commit_message,
  start_branch: train_target_branch_name,
  actions: [{
    action: 'create',
    file_path: changes_file_path,
    content: changes_content,
  }],
  force: true # Potentially unnecessary, if MR is set to remove source branch
}

commit_path = "#{gitlab_host}/api/v4/projects/#{project_id}/repository/commits"
puts commit_path
commit_response = HTTParty.post(
  commit_path,
  headers: auth_headers,
  body: commit_request_body
)

puts commit_response

############################
## Now, open a merge request from the target branch to the train target:

create_mr_request_body = {
  source_branch: commit_target_branch_name,
  target_branch: train_target_branch_name,
  title: "Automatic configuration file update #{Time.now.utc.to_s}",
  allow_collaboration: false,
  approvals_before_merge: 0, # This is deprecated at the moment, but works for now
  remove_source_branch: true # If this is enabled, you won't need force: true in the commit request
}

create_mr_response = HTTParty.post(
"#{gitlab_host}/api/v4/projects/#{project_id}/merge_requests",
  headers: auth_headers,
  body: create_mr_request_body
)

puts create_mr_response

############################
## Now, merge the MR with the skip-train option

sleep 5 # Wait a few seconds for the MR to create and become "mergeable"

mr_iid = create_mr_response['iid']
merge_response = HTTParty.put(
  "#{gitlab_host}/api/v4/projects/#{project_id}/merge_requests/#{mr_iid}/merge",
  headers: auth_headers,
  body: { remove_source_branch: true, skip_merge_train: true } # Or you could also do it here!
)

puts merge_response
