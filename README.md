# skip-the-merge-train

## Getting started

This project is pretty straightforward. `skip-the-train.rb` is a ruby script that you can
run the commit the current state of a local file to a new branch, open a merge request for
that branch, and merge it immediately.

Requires a local installation of ruby and bundle with the ruby version from .tool-versions.

Steps:

1. Make changes to `changes`, and save them.
2. No local git operations are required.
3. Install the dependencies:
```ruby
bundle install
```
4. Copy .env.example to .env and update the environment variables to specify the desired project and host:

```ruby
cp .env.example .env
```

3. Run the script to commit a file and create an MR for it. Then the last api request is responsible for merging the MR without refreshing the rest of the train:

```ruby
ruby skip-the-train.rb
```

## Does it skip the merge train though?

Not yet! The idea is that when the `merge_trains_skip_train` feature flag is enabled,
along with the `merge_trains_skip_train_allowed` CiCdSetting, `skip_merge_train: true`
can be passed in the existing `PUT .../merge` request, and the train should not notice
the new changes having been added.
